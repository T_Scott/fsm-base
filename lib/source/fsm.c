/*
 * fsm.c
 *
 * Created on: 
 * Author: Travis Scott
 */

/************************************************************************************************************************************/
/*                                                             Includes                                                             */
/************************************************************************************************************************************/

//----- Interface Declarations -----//

#include "fsm.h"

//----- Libraries -----//

#include <stdint.h>

//----- Common -----//


//----- Modules -----//
 

//----- Platform Specific -----//


/************************************************************************************************************************************/
/*                                                       Private Definitions                                                        */
/************************************************************************************************************************************/

#define FSM_MAX_STATE_MACHINES 			1

/************************************************************************************************************************************/
/*                                                        Private Structures                                                        */
/************************************************************************************************************************************/

struct sub_state_machine {
	uint8_t current_state;
	uint8_t next_state;
	transfer_function_t* transfer_function_array;
	state_function_t* state_function_array;
	struct task* task_s_array;
	uint8_t num_tasks;
	parse_function_t parse_function;
	struct state_data* state_data_s_ptr;
	increment_millisecond_counter_function_t increment_millisecond_counter_function;
};

/************************************************************************************************************************************/
/*                                                         Local Prototypes                                                         */
/************************************************************************************************************************************/


/************************************************************************************************************************************/
/*                                                         Local Variables                                                          */
/************************************************************************************************************************************/

static uint8_t num_registered_state_machines = 0;

static struct sub_state_machine sub_state_machines_s_array[FSM_MAX_STATE_MACHINES];

/************************************************************************************************************************************/
/*                                                       Interface Functions                                                        */
/************************************************************************************************************************************/


/************************************************************************************************************************************/
/*                                                         Local Functions                                                          */
/************************************************************************************************************************************/


/************************************************************************************************************************************/
/*                                                    Interrupt Service Routines                                                    */
/************************************************************************************************************************************/

int8_t FSM_register(transfer_function_t* transfer_function_array, state_function_t* state_function_array, struct task* task_s_array, uint8_t num_tasks, parse_function_t parse_function, struct state_data* state_data_s_ptr, increment_millisecond_counter_function_t increment_millisecond_counter_function, uint8_t initial_state)
{
	if (num_registered_state_machines == FSM_MAX_STATE_MACHINES) {
		return -1;
	}

	sub_state_machines_s_array[num_registered_state_machines].current_state = 255; // TODO
	sub_state_machines_s_array[num_registered_state_machines].next_state = initial_state;
	sub_state_machines_s_array[num_registered_state_machines].transfer_function_array = transfer_function_array;
	sub_state_machines_s_array[num_registered_state_machines].state_function_array = state_function_array;
	sub_state_machines_s_array[num_registered_state_machines].task_s_array = task_s_array;
	sub_state_machines_s_array[num_registered_state_machines].num_tasks = num_tasks;
	sub_state_machines_s_array[num_registered_state_machines].parse_function = parse_function;
	sub_state_machines_s_array[num_registered_state_machines].state_data_s_ptr = state_data_s_ptr;
	sub_state_machines_s_array[num_registered_state_machines].increment_millisecond_counter_function = increment_millisecond_counter_function;

	num_registered_state_machines++;

	return num_registered_state_machines;
}

void FSM_tick(uint8_t message_type, uint8_t message_bytes, uint8_t* message_ptr)
{
	// Tick each state machine
	for (uint8_t i = 0; i < num_registered_state_machines; i++) {

		uint8_t current_state = sub_state_machines_s_array[i].current_state;
		uint8_t next_state = sub_state_machines_s_array[i].next_state;

		// Allow stateless message parsing
		sub_state_machines_s_array[i].parse_function(sub_state_machines_s_array[i].state_data_s_ptr, message_type, message_bytes, message_ptr);

		// If we are changing state, call the transfer function
		if (current_state != next_state) {
			sub_state_machines_s_array[i].transfer_function_array[next_state](sub_state_machines_s_array[i].state_data_s_ptr, current_state);
		}

		// Next state will become current state
		sub_state_machines_s_array[i].current_state = next_state;

		// Run the next state, making it the current state, and retrieving the next state
		sub_state_machines_s_array[i].next_state = sub_state_machines_s_array[i].state_function_array[next_state](sub_state_machines_s_array[i].state_data_s_ptr, message_type, message_bytes, message_ptr);

		// Run any scheduled tasks
		for (uint8_t j = 0; j < sub_state_machines_s_array[i].num_tasks; j++) {
			if (sub_state_machines_s_array[i].task_s_array[j].scheduled == true) {
				sub_state_machines_s_array[i].task_s_array[j].task_function(sub_state_machines_s_array[i].state_data_s_ptr, sub_state_machines_s_array[i].current_state);
				sub_state_machines_s_array[i].task_s_array[j].scheduled = false;
			}
		}
	}
}

void FSM_run_millisecond_task_scheduler(void)
{
	for (uint8_t i = 0; i < num_registered_state_machines; i++) {

		sub_state_machines_s_array[i].increment_millisecond_counter_function();

		for (uint8_t j = 0; j < sub_state_machines_s_array[i].num_tasks; j++) {
			sub_state_machines_s_array[i].task_s_array[j].millisecond_counter_v++;
			if (sub_state_machines_s_array[i].task_s_array[j].millisecond_counter_v == sub_state_machines_s_array[i].task_s_array[j].period_milliseconds) {
				sub_state_machines_s_array[i].task_s_array[j].millisecond_counter_v = 0;
				sub_state_machines_s_array[i].task_s_array[j].scheduled = true;
			}
		}
	}
}