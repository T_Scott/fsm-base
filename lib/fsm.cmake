########## Files ##########

# Set and add include locations
set(INCLUDES ${CMAKE_CURRENT_LIST_DIR}/include)
include_directories(${INCLUDES})

# Set sources
FILE(GLOB SOURCES
	${CMAKE_CURRENT_LIST_DIR}/source/fsm.c
)

########## Outputs ##########

# Add library
add_library(fsm ${SOURCES})
set(OPTIONAL_LIBS ${OPTIONAL_LIBS} fsm)