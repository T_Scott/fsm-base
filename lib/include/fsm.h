/*
 * fsm.h
 *
 * Created on: 
 * Author: Travis Scott
 */

#ifndef FSM_H
#define FSM_H

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************************************************************************/
/*                                                     Header Required Includes                                                     */
/************************************************************************************************************************************/

//----- Libraries -----//

#include <stdint.h>
#include <stdbool.h>

//----- Common -----//


//----- Modules -----//


//----- Platform Specific -----//


/************************************************************************************************************************************/
/*                                                       Public Definitions                                                         */
/************************************************************************************************************************************/

#define FSM_TRANSFER_PROTOTYPE(name)			void transfer_ ## name (struct state_data* state_data_s_ptr, uint8_t previous_state)	
#define FSM_STATE_PROTOTYPE(name)				uint8_t state_ ## name (struct state_data* state_data_s_ptr, uint8_t message_type, uint8_t message_bytes, uint8_t* message_ptr)	
#define FSM_TASK_PROTOTYPE(name)				void task_ ## name (struct state_data* state_data_s_ptr, uint8_t current_state)

/************************************************************************************************************************************/
/*                                                        Public Structures                                                         */
/************************************************************************************************************************************/

struct state_data;

typedef void (*transfer_function_t)(struct state_data* state_data_s_ptr, uint8_t previous_state);
typedef uint8_t (*state_function_t)(struct state_data* state_data_s_ptr, uint8_t message_type, uint8_t message_length, uint8_t* message_ptr);
typedef void (*task_function_t)(struct state_data* state_data_s_ptr, uint8_t current_state);
typedef void (*parse_function_t)(struct state_data* state_data_s_ptr, uint8_t message_type, uint8_t message_bytes, uint8_t* message_ptr);

struct task {
	task_function_t task_function;
	uint32_t period_milliseconds;
	volatile uint32_t millisecond_counter_v;
	bool scheduled;
};

typedef void (*increment_millisecond_counter_function_t)(void);

/************************************************************************************************************************************/
/*                                                         Global Variables                                                         */
/************************************************************************************************************************************/


/************************************************************************************************************************************/
/*                                                       Interface Prototypes                                                       */
/************************************************************************************************************************************/

int8_t FSM_register(transfer_function_t* transfer_function_array, state_function_t* state_function_array, struct task* task_s_array, uint8_t num_tasks, parse_function_t parse_function, struct state_data* state_data_s_ptr, increment_millisecond_counter_function_t increment_millisecond_counter_function, uint8_t initial_state);

void FSM_tick(uint8_t message_type, uint8_t message_bytes, uint8_t* message_ptr);

void FSM_run_millisecond_task_scheduler(void);

#ifdef __cplusplus
}
#endif

#endif /* FSM_H */